"""Importing Argparse module and excercise code."""
import argparse
import transcript_sampler as ts


def main():
    """[Parse the arguments through command line]."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--ID", nargs="+",
                        type=int,
                        default=[23, 12],
                        help="Enter the gene-IDs")
    parser.add_argument("--COP",
                        nargs="+",
                        type=int,
                        default=[50, 60],
                        help="Enter the copies")

    args = parser.parse_args()

    print(args.ID)
    print(args.COP)

    data = dict(zip(args.ID, args.COP))

    print(data)

    data2 = ts.TranscriptSampler.sample_transcripts(data, 800)

    ts.TranscriptSampler.write_sample(data2)


if __name__ == "__main__":
    main()
else:
    pass
