"""Numpy imported in order to do the sampling."""
import numpy as np


if __name__ == "__main__":
    pass

else:

    class TranscriptSampler:
        """This class contains the solution to the three functions."""

        # First task
        # myfile = open("/Users/kegany44/Master/PythonCourse/IDnCOPIES.txt",
        # "r")

        def read_avg_expression(myfile):
            """[Create a dictionary out of a txt file].

            Returns:
                [idcopies (dictionary)]: [returns the dictionary with
                the IDs as the keys
                and the average number as the corresponding values]
            """
            mylist = []

            for myline in myfile:

                if myline[0].isdigit():
                    mylist.append(myline)
                else:
                    continue

            mylist_2 = []

            for k in range(0, len(mylist)):
                seclist = mylist[k].split()
                mylist_2.append(seclist)

            idcopies = {}

            for i in range(0, len(mylist_2)):

                idcopies[int(mylist_2[i][0])] = int(mylist_2[i][1])

            myfile.close()

            return idcopies

        # data = read_avg_expression(myfile)

        def sample_transcripts(avgs, number):
            """[Functions which does the sampling].

            Args:
                avgs ([dictionary]): [contains the gene IDs and the
                corresponding copy number]
                number ([int]): [number of transcipts to sample]

            Returns:
                [type]: [description]
            """
            values = avgs.values()
            val_sum = sum(values)

            rel_ab_list = []

            for (k, v) in avgs.items():
                rel_ab = v/val_sum
                rel_ab_list.append(rel_ab)

            # print(rel_ab_list)

            list_k = []

            for key in avgs.keys():
                list_k.append(key)

            # print(list_k)

            # sampling\n",

            counts = np.random.choice(list_k, number, True, rel_ab_list)

            occurences = {}

            for k in list_k:
                a = np.count_nonzero(counts == k)
                occurences[k] = a

            return occurences

        # data2 = sample_transcripts(data, 800)

            # Third task\n",
        def write_sample(sample):
            """[Write the result into a file].

            Args:
                sample ([dict]): [contains the sampling data]

            Returns:
                [file]: [contains the file with the data]
            """
            result_file = open('Result_+', 'w+')
            for key, value in sample.items():
                result_file.write('%s:%s\\n' % (key, value))

            result_file.close()

            return result_file

        # write_sample(data2)
